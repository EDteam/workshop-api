<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\Validator;
use File;
use Storage;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ['id' => 1, 'nombre' => 'Laravel desde cero', 'tipo' => 'libro', 'costo' => '40USD'];

        return response()->json(['data' => $data, 'code' => 200]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function storeLocal(Request $request) {
      //name, file
      ### Validator::make, errors, fails

      $validator = Validator::make($request->json()->all(), [
        'name' => 'required|min:5',
        'filename' => 'max:500000' #Kilobytes. Max: 10240 = Max 10 MB
        #'filename' => 'dimensions:min_width=100,min_height=200',
        #'filename' => 'mimes:jpeg,png',
        #'filename' => 'required|max:500000|dimensions:min_width=100,min_height=200'
        #'filename' => 'required|max:500000|mimes:doc,docx,pdf,jpg'
      ]);

      if ($validator->fails()) {
        #No me dejes insertar, me envíes el error en respuesta json
        return response()->json(
          ['data' => $validator->errors(), 'code' => 404]
        );
      } else {
        #Subir el archivo a mi disco local, haga la inserción a nuestra tabla Product.

        #1. Subir el archivo
        $filename = $request->file('filename');
        $extension = $filename->getClientOriginalExtension(); #La extensión del archivo que estoy subiendo
        $fName = $filename->getFilename().'.'.$extension;
        Storage::disk('public')->put($fName, File::get($filename));

        #2. Inserción en nuestra tabla
        $product = new Product;
        $product->name = $request->name;
        $product->filename = $fName;
        $product->save();

        return response()->json(
          ['data' => 'Inserción correcta', 'code' => 200]
        );

      }

    }

    public function storeAmazon(Request $request) {
      $validator = Validator::make($request->json()->all(), [
        'name' => 'required|min:5',
        'filename' => 'max:500000'
      ]);

      if ($validator->fails()) {
        return response()->json(
          ['data' => $validator->errors(), 'code' => 404]
        );
      } else {
        $filename = $request->file('filename');
        $extension = $filename->getClientOriginalExtension();
        $fName = $filename->getFilename().'.'.$extension;
        Storage::disk('s3')->put($fName, File::get($filename), 'public');

        $product = new Product;
        $product->name = $request->name;
        $product->filename = $fName;
        $product->save();

        return response()->json(
          ['data' => 'Inserción correcta', 'code' => 200]
        );

      }
    }
}
