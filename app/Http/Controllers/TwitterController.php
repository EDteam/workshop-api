<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Twitter;
use Carbon\Carbon;
use App\Tweet;
use App\Notifications\TweetPublished;
use Illuminate\Support\Facades\Notification;

class TwitterController extends Controller
{
    // $count, $screenName (edteamlat)
    public function timeline($count, $screenName) {
        return Twitter::getUserTimeline([
            'screen_name' => $screenName,
            'count' => $count,
            'format' => 'json'
        ]);
    }

    public function search($count, $search) {
      try {
          return Twitter::getSearch([
              'q' => $search, 'count' => $count, 'format' => 'json'
          ]);
      } catch (\Exception $e) {
          return $e->getMessage();
      }
    }

    public function saveTweet(Request $request) {

      $tweet = Tweet::create([
        'account' => $request->input('account'),
        'tweet' => $request->input('tweet'),
        'tweetead_at' => $request->input('tweeted_at'),
      ]);

      $message = Tweet::whereDate('tweeted_at', Carbon::today())->first();
      $message->notify(new TweetPublished());
      $message->delete();

      return redirect()->back();


    }



}
